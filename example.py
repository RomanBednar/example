#!/usr/bin/python
"""
This tool will serve as a wrapper for managing test runs. It is an interface between legacy perl test drivers
and it's goal is to change how we handle testing and metadata so it meets the latest requirements and is
ready for upcoming requirements like CI/CD, cloud schedulers and so on.


Situation:  Metadata have a crucial role in CI/CD in terms of automated test runs, as they specify what requirement
            each test has (cpu, ram, disks....)
            Rest of the team is using python library for generating metadata, rewriting the same logic into perl
            would be redundant and expensive.

Solution:   Metadata are represented as a quite complex xml structure for which we already implemented a library.
            Instead of duplicating this logic into perl tools make each of the 'driver' scripts dump a json structure,
            parse it using this script and finally use a library for generating the metadata which is common for the
            whole team.

Current features:
1) ability to register perl test drivers
2) validating that drivers report json data
3) using defaults where metadata is missing (temporary)
4) use metadata library to generate xml with all metadata


Features to be added:
1) test listing and execution
2) capability to show overview of tests (test plan details)


"""

import argparse
import logging
import json
import sys
import os

from copy import deepcopy

DEFAULT_LOG_LEVEL = logging.DEBUG
logging.getLogger().setLevel(DEFAULT_LOG_LEVEL)

parser = argparse.ArgumentParser(description='LVM2 test driver and metadata parser.')

group1 = parser.add_argument_group('STS related parameters.')
group1.add_argument(
    '-R',
    '--resource-file',
    dest='resourcefile',
    nargs=1,
    metavar='STS_RESOURCE_FILE',
    help='resource file for the cluster we want to run on')

group1.add_argument(
    '-l',
    '--local-sts-root',
    dest='lroot',
    metavar='STS_LROOT',
    nargs=1,
    help='local root of sts installation')
group1.add_argument(
    '-r',
    '--remote-sts-root',
    dest='rroot',
    metavar='STS_RROOT',
    nargs=1,
    help='remote root of sts installation')
group1.add_argument(
    '--lvm-bin-dir',
    dest='lvm_bin_dir',
    default='lvm2/bin',
    nargs=1,
    help='path to lvm binaries relative to sts lroot')

group2 = parser.add_argument_group('Options.')
group2.add_argument(
    '-S',
    '--metadata',
    dest='dump_meta',
    action='store_true',
    help='Load metadata from all drivers and dump them to valid vedder xml.')


def parse_sts_values(cmdl_args):
    """
    check whether resourcefile, rroot or lroot were given on commandline
    if not check whether in ENV
    if not guess it for lroot and rroot
    resourcefile absence ends the run sts common values

    :param cmdl_args: option parser object

    Returns:
            lroot, rroot, resourcefile
    """
    if cmdl_args.resourcefile is not None:
        resourcefile = cmdl_args.resourcefile
    else:  # resourcefile given in ENV
        if os.environ.get('STS_RESOURCE_FILE') is not None:
            resourcefile = os.environ.get('STS_RESOURCE_FILE')
            logging.info('STS_RESOURCE_FILE used from ENV: %s\n' % resourcefile)
        else:  # not given on command line nor in ENV
            resourcefile = None

    # end if resourcefile is not readable
    if resourcefile is not None:
        if not os.access(resourcefile, os.R_OK):
            logging.error('\nError: Resource file (%s) must be readable file.\n' % resourcefile)
            sys.exit(2)

    if cmdl_args.lroot is None:
        if 'STS_LROOT' in os.environ:
            lroot = os.environ['STS_LROOT']
        # presume that the script reside in $STS_ROOT/pacemaker
        else:
            lroot = os.path.abspath(
                os.path.join(
                    os.path.abspath(__file__),
                    '../../..'))
    else:
        lroot = cmdl_args.lroot

    if cmdl_args.rroot is None:
        if 'STS_RROOT' in os.environ:
            rroot = os.environ['STS_RROOT']
        # presume that the script reside in $STS_ROOT/pacemaker
        else:
            rroot = os.path.abspath(
                os.path.join(
                    os.path.abspath(__file__),
                    '../../..'))
    else:
        rroot = cmdl_args.rroot
    return lroot, rroot, resourcefile


opt = parser.parse_args()
# assign common STS values
opt.lroot, opt.rroot, opt.resourcefile = parse_sts_values(opt)

# try to import QAlib
sys.path.append(os.path.join(opt.lroot, 'lib'))
try:
    from QAlib.ScenariosMetadata import ScenariosMetadata, QAlibMetadata
    from QAlib.common import shell, QAShellException
except Exception as e:
    sys.stderr.write(
        'Unexpected exception occurred during import of QAlib:\n%s\n' % e
    )
    sys.exit(8)


class MetaParser(object):
    """
    During MetaParser init:

    1) drivers_list is picked up by _drivers setter - each driver is checked that it provides a valid json when called
        with a flag specified in `self.metadump_flag`

    2) `self._get_drivers_meta` is an entry point for loading all metadata which gathers all the metadata and tries to
        finalize it using `self._check_and_complete_metadata`. The finalization process is rather complex as it checks
        'mandatory' meta values and attempts to assign defaults where reasonable if some values are missing from
        `self.defaults`.
    """

    def __init__(self, opt, drivers_list):
        self.lvm_bin_path = os.path.join(opt.lroot, opt.lvm_bin_dir)
        self.metadump_flag = '-S'  # this flag should make every driver dump all metadata to json
        self.required_values = ['name', 'distro', 'variant', 'tags', 'comment', 'env_req', ]
        self.defaults = {
            'comment': (
                "Comment not provided for this scenario."
            ),
            'distro': 'RHEL7',
            'variant': 'default',
            'tags': [
                'qa-level:1', 'tier1', 'component:lvm2', 'suite:lvm2'
            ],
            'env_req': {
                'required': {
                    'shared': {
                        'devices': {
                            'disks': {
                                'summary': {
                                    'count': {
                                        'min': '10'
                                    },
                                    'size': {
                                        'min': '30'
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        self._drivers = drivers_list
        self.drivers_meta = self._get_drivers_meta()

    @property
    def drivers(self):
        return self._drivers

    @drivers.setter
    def drivers(self, drivers):
        """
        Verify and then register lvm test driver. Each test driver has to provide -S switch to dump all metadata as json

        :param drivers: lvm script driver/binary

        :return: True if driver provides metadata, else exit
        """

        for d in drivers:
            driver_bin = os.path.join(self.lvm_bin_path, d)
            cmd = '%s %s 2> /dev/null' % (driver_bin, self.metadump_flag)
            logging.debug('Testing if driver %s provides json.' % d)
            try:
                meta_json = shell(cmd)
                json.loads(meta_json)
            except (QAShellException, ValueError) as e:
                logging.error('Failed to get json metadata from driver %s' % d)
                logging.error('ERROR: %s' % e)
                sys.exit(1)

            self._drivers.append(d)
            logging.debug('Successfully registered driver %s' % d)

    def _get_drivers_meta(self, drivers=None):
        """
        Get metadata from all drivers.

        :param drivers (optional): list of drivers for which to get metadata

        :return: metadata dictionary structure from all drivers
                 Example:
                 {
                 'driver' : ['meta' : 'values'],
                 'driver2' : ['meta2' : 'values2'],
                 }
        """
        meta_all = {}

        if drivers is None:
            drivers = self.drivers

        for d in drivers:
            driver_bin = os.path.join(self.lvm_bin_path, d)
            cmd = '%s %s 2> /dev/null' % (driver_bin, self.metadump_flag)

            try:
                meta_json = shell(cmd)
            except QAShellException:
                logging.error('Failed to get metadata from driver %s' % d)
                return False
            meta_all[d] = []
            meta_all[d].append(json.loads(meta_json))

        return self._check_and_complete_metadata(meta_all)

    def _custom_env_req(self, requirements):
        """
        Method for use with custom requirements, like disk count and size.
        This is likely to be expanded in the future if more requirements are needed in scenarios meta

        :param requirements: dictionary with requirements, allowed values are defined in :var allowed_req

        :return: env_req structure for `ScenariosMetadata.add_test()` method. False on failure
        """
        allowed_req = ['disk_count', 'disk_size']

        env_req_copy = deepcopy(self.defaults['env_req'])

        for requirement in requirements.keys():
            if requirement not in allowed_req:
                logging.error('Unknown requirement: %s' % requirement)

        env_req_copy['required']['shared']['devices']['disks']['summary']['count']['min'] = requirements['disk_count']
        env_req_copy['required']['shared']['devices']['disks']['summary']['size']['min'] = requirements['disk_size']

        return env_req_copy

    def _get_default_value(self, entry):
        """
        Gets default value for given entry from `self.defaults`

        :param entry: name of metadata entry to get defaults for
        :return: default value for entry
        """

        allowed_values = self.defaults.keys()

        if entry not in allowed_values:
            logging.error('Attempted to lookup default value for \'%s\' but it is not defined in this object.' % entry)
            logging.error('Allowed values for default lookup are: %s' % allowed_values)
            raise ValueError
        else:
            return self.defaults[entry]

    def _check_and_complete_metadata(self, meta):
        """
        Check provided metadata for required values, exit if some values are not present.

        Complete missing parts with defaults where it makes sense from `self.defaults`
        Some missing metadata in scenario are tolerable (e.g. comment) some not (e.g. disk size and count)

        :return: Verified and completed metadata with defaults, False on failure
        """

        completed_meta = {}
        requirements = {}

        # go over each driver in metadata (keys are driver names)
        for driver_name in meta.keys():
            logging.debug('Verifying metadata provided by: %s' % driver_name)
            for test_metadata in meta[driver_name]:
                for command in test_metadata:
                    logging.debug('\n-->Current command: %s' % command)

                    try:
                        # detect if scenario specified disk_count
                        if test_metadata[command]['disk_count']:
                            logging.debug('Custom disk count detected.')
                            requirements['disk_count'] = test_metadata[command]['disk_count']  # save it as requirement

                        # detect if scenario specified disk_size
                        if test_metadata[command]['disk_size']:
                            logging.debug('Custom disk size detected.')
                            requirements['disk_size'] = test_metadata[command]['disk_size']  # save it as requirement
                    except KeyError:
                        # TODO: die if scenario is missing disk info once meta rework is considered finished
                        logging.warning(10 * '#' + 'WARNING' + 10 * '#')
                        logging.warning(
                            'disk_size/disk_count missing in scenario: %s ' % test_metadata[command]['name'])
                        logging.warning(27 * '#')

                    # look for required metadata values in each scenario/command
                    for value in self.required_values:
                        logging.debug('Parsing \'%s\' value in %s metadata' % (value, driver_name))

                        # if parsing env_req and some requirements are set already use a method that changes
                        # default env_req based on requirements
                        if value == 'env_req' and len(requirements) > 0:
                            logging.debug('Using customized environment because some requirements were specified')
                            test_metadata[command]['env_req'] = self._custom_env_req(requirements)

                        # deal with values that are required but might be assigned some defaults
                        if value not in test_metadata[command].keys():
                            try:
                                logging.debug('\'%s\' value was not found, try to load default' % value)
                                default_value = self._get_default_value(value)
                                test_metadata[command][value] = default_value
                                logging.debug(
                                    'Assigning default value of \'%s\' to meta entry \'%s\'' % (default_value, value))

                            # can't find required metadata and can't assign reasonable defaults -> exit
                            except ValueError:
                                logging.error(
                                    '\'%s\' is a required metadata value but was not found in this scenario:\n%s' % (
                                        value, test_metadata[command]))
                                logging.error('Command: %s\n' % command)
                                logging.error('Aborting.\n')
                                sys.exit(1)

                completed_meta[driver_name] = []
                completed_meta[driver_name].append(test_metadata)

        return completed_meta


if __name__ == '__main__':

    # set drivers for MetaParser
    drivers_list = ['snapper', ]  # -> add all lvm drivers here
    mp = MetaParser(opt, drivers_list)

    # prepare ScenariosMetadata
    sm = ScenariosMetadata('lvm_meta')
    sm.set_auto_defaults(False)

    # recursively add tests
    for driver_name in mp.drivers_meta:
        logging.debug('Parsing meta from driver: %s' % driver_name)
        for meta in mp.drivers_meta[driver_name]:
            for command in meta:
                sm.add_test(
                    command=command,
                    name=meta[command]['name'],
                    provides=meta[command]['provides'],
                    comment=meta[command]['desc'],
                    requires=meta[command]['requires'],
                    requires_post=meta[command]['requires_post'],
                    distro=meta[command]['distro'],
                    variant=meta[command]['variant'],
                    tags=meta[command]['tags'],
                    env_req=meta[command]['env_req'],
                )

    if opt.dump_meta:
        print(sm.show_xml())
